# Game Design Document

*Authored by: Stian and Viktor*
*Repo: https://git.gvk.idi.ntnu.no/ViktorPalmason/double-trouble*
*Template: [A GDD Template for the Indie developer](https://www.gamasutra.com/blogs/JasonBakker/20090604/84211/A_GDD_Template_for_the_Indie_Developer.php) 22.08.2020*

## Idea 1: Satan's fight pit ✅ 👍

### 1.1 The Pitch
A 3D-platformer brawl game, where you play as one of the tortured souls in hell that fights in Satan’s pit for the chance of winning the prize that all the souls in hell wish for the most. To return to earth. To win you, alone or with a friend, must fight in Satan’s pit and gather points by completing the objectives that spawn inside the pit while battling against other tortured souls that are trying to do the same.

### 1.2 Game type
- 3D platformer
- Brawl
- Fighting
- Supernatural

### 1.3 Inspiring games
- Luigis Mansion
- Mario Kart
- Playing with fire 2 - https://www.crazygames.com/game/playing-with-fire-2
- Fall guys - https://store.steampowered.com/app/1097150/Fall_Guys_Ultimate_Knockout/
- PUBG - https://store.steampowered.com/app/578080/PLAYERUNKNOWNS_BATTLEGROUNDS/


## 2. The plot
Satan is getting bored and wants some entertainment, so he gets the souls of most evil humans to fight each other for a ticket to be put back on earth. What they do not know is that Satan has gained control of earth, so they immediately die when they are brought back to earth. When they return to Hell their memory of the fight and the lie that they face on earth is erased and wake up in hell ready to fight again for an escape that never lasts. The losers of the fight are put back into their pits to wait for the next fight.

## 3. Gameplay description
A 3D-platformer battleground/brawl game, where you alone or with a friend battle against other players for points to win the match. 
-	There 2-4 players in a match
-	The first player who gets 4 points wins, but a player must always win with at least a two point lead so if there is a player with 3 points then the winning player has to get the 4th point, which will be called an advantage, and then another final point to obtain the two point lead winning condition. (Winning condition is taken from the point system in Tennis).
    -	When advantage points are in play then the player with the advantage is given an extra power to be able to finish the game, and if the player fails to complete the next objective or gets knocked out he loses the advantage and therefore the powerup.
        -	The powerup will not be too strong so that the other player/s still has/have a chance to win. 
        -	After some time has passed the powerups gets more powerful to ensure the game does not run for too long.

You gather points through doing objectives that spawn on the playing field.
-	The objectives that spawn on the board are only there for a certain amount of time and then they disappear, so players must gather as many points before they disappear. 

Battle is done mostly in close combat with punches and kicks, and players can knock out other players for a few seconds.
-	Players have a health bar that lowers as they take damage and when the health bar is at 0 the player is knocked out for a few seconds and then they can continue fighting. 

There are also possibilities for other types of combat style through items that they can collect on the map and with ultimate attacks gained through combat energy. (EXTRA)
- During the match crates will appear and when broken they will give you and item to fight with.
- Each player has an energy bar which is filled when successful attacks are performed against other players. When that bar is full the player can perform an ultimate attack.
    - An extra method to gain combat energy is getting them by completing objectives.

Playable characters: WIP. Classes, default character for everyone or custom setup at main menu.
## 4. Artistic Style Outline
- Demonic
- Horror
- Intense
- Hot (Hot colours)

## 5. Systematic Breakdown of Components

### 5.6 Graphics / 3D rendering
- Unity handles the rendereing

### 5.1 GUI system
- Use unity for rendering GUI
- Need clickable buttons
- Display health, energy meter?
- Display abilities/cooldowns/powerUP!
- Chat system
- Text
- Pause Menu

### 5.2 Collision and physics system
- Unity will handle collision and physics

### 5.3 AI System
- AI opponents, in case you dont have 4 friends to play with.
- Pretty straight forward. They should be like playing against humans. (haha! good luck)

### 5.6 Game/Level system
- Spawn of players.
- Spawning of objectives.
- Spawning of items.
- Point keeping
- Time keeping
- Health energy management
- Transitions:
    - Game begin
    - Game end
    - Game running
    - Game pause 

### 5.4 Animations
- Unity animator handles the animations
- React to events
    - Running / movement
    - Punching / fighting
    - Camera movement
    - PowerUP
    - KnockOUT / KO
    - Spawning things
    - Open/close menu
- Keep track of durations of animations.

### 5.5 Input / Events
- Keyboard controller
- Gamepad controller (optional, stretch goal)
- Network controller
- AI controller
- Timer/Clock events
- Map Controller 1,2,3,4 ---> Players 1,2,3,4

### 5.7 Multiplayer/Networking
- Player invite event
- Player join event
- Player left event
- Player actions (powerUP, KO could be computed by each client)
- Player chat message in/out
- Game events Master --> Slave1, slave2, slave3

### 5.8 File system Save/load 
- History? Wins/losses/experience points/level?
- Amount of damage done:
    - All time
    - Since satans last birthday
    - last week
    - last month
- Friend list:
    - Remember friends, so easy to invite again.
- Cosmetics
    - Hats?
    - Colors?
- Save settings
- Save as JSON or Unity Save system(?)

### 5.9 Sound system
- Unity audio manager will handle the sound
- Background music
    - Menu
    - level
- Sound effects when special events happens
    - Walking
    - Punching (including Special attack)
    - Spawning
    - KO'ing
    - Power Up, Power Down
    - Game Over/Ending

### 5.10 Settings
- Sound level
- Resolution
- As few settings as possible

## 6. Assets Breakdown

### 6.1 Art Assets
- Medieval Cartoon Heros from unity assets store
https://assetstore.unity.com/packages/3d/characters/medieval-cartoon-warriors-90079
### 6.2 Text Assets
### 6.3 Sound Assets
- Possibly find something in the unity assets store.
- Pow
- Aow
- K/Ohhhh
- Menu music
- Level music
- Spawn sound
- Walking click/clack/click/clack
- Satan sounds / evil laugh.
- Torture
## 7. Game Flow Diagram

Example:  (ie. Start Game -> Cutscene -> Tutorial -> loop(Cutscene -> Level -> Results Screen) -> End)

## 8. Development process

### 8.1 Teamwork
### 8.2 Timeline
### 8.3 Testing
### 8.4 Documentation
### 8.5 Hardware requirements
### 8.6 Tech stack

## 9. Misc

## 10. References

