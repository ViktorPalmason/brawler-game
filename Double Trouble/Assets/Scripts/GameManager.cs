﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool _gameOver = false;

    private void Update()
    {
        if (_gameOver)
        {
            if (Input.GetKey(KeyCode.R))
            {
                SceneManager.LoadScene(1); // Load in the Game scene
            }
            else if (Input.GetKey(KeyCode.Q))
            {
                SceneManager.LoadScene(0); // Load in the Main menu scene
            }
        }
    }

    public void GameOver()
    {
        _gameOver = true;
    }
}
