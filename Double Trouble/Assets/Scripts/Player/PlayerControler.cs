﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    [SerializeField] private float _movementSpeed = 10f;
    [SerializeField] private float _jumpForce = 10f;
    [SerializeField] private int _score;
    [SerializeField] private int _scoreToWin = 50;
    [SerializeField] private int _maxHealth = 20;
    [SerializeField] private int _currentHealth;
    [SerializeField] private Healthbar _healthbar;
    [SerializeField] private PlayerUI _UI;
    [SerializeField] private PlayerAudio _audio;
    [SerializeField] private Character _character;

    private Vector3 _movementDirection;
    private Quaternion _right = Quaternion.Euler(0f, 90f, 0f);
    private Quaternion _left = Quaternion.Euler(0f, -90f, 0f);
    private Animator _animator;
    private Sword _sword;
    private bool _canAttack = true;
    private CharacterController _controller;
    [SerializeField] private bool _isGrounded;
    private float _yVelocity;
    [SerializeField] private float _gravity = 0.3f;
    [SerializeField] private bool _isRunning = false;
    private bool _isAlive = true;
    private bool _gameOver = false;

    private void Start()
    {
        _currentHealth = _maxHealth;
        _isAlive = true;
        _score = 0;
        
        if(_healthbar == null)
            Debug.LogError("(PlayerControler)Start: Health bar is NULL");
        else
            _healthbar.SetMaxHealth(_maxHealth);

        _animator = GetComponent<Animator>();
        if(_animator == null)
            Debug.LogError("(PlayerControler)Start: Animator is NULL");

        _sword = GameObject.Find("Sword_3").GetComponent<Sword>();
        if (_sword == null)
            Debug.LogError("(PlayerControler)Start: Sword_3 is NULL");

        if(_UI == null)
            Debug.LogError("PlayerControler)Start: UI is NULL");

        if (_audio == null)
            Debug.LogError("(PlayerControler)Start: The Audio is NULL");

        if (_character == null)
            Debug.LogError("(PlayerControler)Start: The Character is NULL");

        _controller = GetComponent<CharacterController>();
        if (_controller == null)
            Debug.LogError("(PlayerControler)Start: The Character controller is NULL");
    }

    private void Update()
    {
        if(_isAlive && !_gameOver)
        {
            /// The _controller.isGrounded gives unstable returns because of the asset animation of the player
            /// so I store the true value when ever isGrounded is true.
            if (_controller.isGrounded)
            {
                _isGrounded = true;
                _yVelocity = 0f;
                _animator.SetBool("Jumping", false);
            }

            MoveCharacter();

            if (_canAttack)
            {
                /// Light attack animation on.
                if (Input.GetKeyDown(KeyCode.Mouse0))
                    StartCoroutine(AttackRoutine("LightAttack", 0.25f));

                /// Heavy attack animation on.
                if (Input.GetKeyDown(KeyCode.Mouse1))
                    StartCoroutine(AttackRoutine("HeavyAttack", 0.55f));
            }
        }
    }
    private void FixedUpdate()
    {
        if (!_isGrounded)
            _yVelocity -= _gravity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Tile" || other.tag == "Ground")
        {
            _isGrounded = true;
            _yVelocity = 0f;
            _animator.SetBool("Jumping", false);
        }
    }
    private void MoveCharacter()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        _movementDirection = new Vector3(horizontalInput, 0, 0) * _movementSpeed;

        /// Smart code that i am happy with to keep the player 0 on the z axis.
        if (transform.position.z > 0 || transform.position.z < 0)
            _movementDirection.z = -transform.position.z * 10;

        /// If 'A' is being pressed and D is not, rotate the player to the left.
        /// Note: If 'D' is being pressed the player cannot turn left even when pressing 'A' 
        /// until 'D' is released
        if (Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
            transform.rotation = _left;
        /// If 'D' is being pressed and A is not, rotate the player to the right.
        /// Note: If 'A' is being pressed the player cannot turn right even when pressing 'D' 
        /// until 'A' is released
        else if (Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A))
            transform.rotation = _right;

        /// If the player is Grounded it can start running or jump
        if (_isGrounded)
        {
            switch (_isRunning)
            {
                case false:
                    /// If either A or D are pressed, start running.
                    if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
                    {
                        _animator.SetBool("isMoving", true);
                        _audio.PlayRunningLoop();
                        _isRunning = true;
                    }
                    break;
                case true:
                    /// If neither A or D are being pressed, stop running.
                    if (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
                    {
                        _animator.SetBool("isMoving", false);
                        _audio.StopRunningLoop();
                        _isRunning = false;
                    }
                    break;
            }

            /// If Space is pressed, add force to the y direction so the player jumps
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _audio.StopRunningLoop();
                _isGrounded = false;
                _isRunning = false;
                _animator.SetBool("Jumping", true);
                _animator.SetBool("isMoving", false);
                _yVelocity = _jumpForce;                
                _audio.PlayJump();                
            }
        }

        _movementDirection.y = _yVelocity;

        _controller.Move(_movementDirection * Time.deltaTime);
    }

    public void TakeDamage(int damage, Vector3 knockback)
    {
        _isRunning = false;
        _animator.SetBool("isMoving", false);
        _canAttack = false;

        _currentHealth -= damage;
        _UI.AddChildDamageText(damage.ToString());
        _healthbar.SetHealth(_currentHealth);
        _audio.StopRunningLoop();

        if (_currentHealth <= 0)
            PlayerDeath(knockback);
        else
            StartCoroutine(KnockBackRoutine(knockback));        
    }
    /// <summary>
    /// Knockback the player in the direction of 'knockback'
    /// </summary>
    /// <param name="knockback"></param>
    private void TakeKnockback(Vector3 knockback)
    {
        if(knockback.x < 0)
            transform.rotation = _right;
        else
            transform.rotation = _left;

        _controller.Move(knockback);
    }

    private void PlayerDeath(Vector3 knockback)
    {        
        _isAlive = false;
        _animator.SetBool("isDead", true);
        TakeKnockback(knockback);
        _audio.PlayDeathScream();
        _UI.EndGameSequence("Death");
    }

    private void PlayerIsKilled()
    {
        Destroy(this.gameObject);
    }

    public void GivePlayerPoints(int points)
    {
        _score += points;
        _UI.UpdateScoreText(_score.ToString());
        if(_score >= _scoreToWin)
        {
            _gameOver = true;
            _UI.EndGameSequence("Win");
        }
    }

    IEnumerator KnockBackRoutine(Vector3 knockback)
    {
        TakeKnockback(knockback);
        _audio.PlayHurtGrunt();
        _animator.SetBool("knockback", true);

        yield return new WaitForSeconds(0.5f); 
        
        _animator.SetBool("knockback", false);
        _character.KnockbackDone();
        _canAttack = true;
    }
    IEnumerator AttackRoutine(string attackType, float timer)
    {
        _canAttack = false;
        _sword.AttackActivate(attackType);
        _animator.SetBool(attackType, true);
        _audio.PlayLightSwordSwing();        

        yield return new WaitForSeconds(timer);
        
        _sword.AttackDeactivate(attackType);
        _animator.SetBool(attackType, false);
        _canAttack = true;
    }
}

