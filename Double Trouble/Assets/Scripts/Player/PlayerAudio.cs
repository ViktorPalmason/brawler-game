﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    [SerializeField] private AudioSource _runningAudioSource, _combatAudioSource, _jumpingAudioSource;
    [SerializeField] private AudioClip _lightSwordSwingAudioClip, _heavySwordSwingAudioClip;
    [SerializeField] private AudioClip _playerDeathAudioClip, _playerHurtAudioClip, _playerAttackGruntAudioClip;

    // Start is called before the first frame update
    void Start()
    {
        if(_runningAudioSource == null)
        {
            Debug.LogError("(PlayerAudio)Start: The Running Audio Source is NULL");
        }

        if(_combatAudioSource == null)
        {
            Debug.LogError("(PlayerAudio)Start: The Combat Audio Source is NULL");
        }
    }

    public void PlayRunningLoop() { _runningAudioSource.Play(); }

    public void StopRunningLoop() { _runningAudioSource.Stop(); }

    public void PlayLightSwordSwing() { _combatAudioSource.PlayOneShot(_lightSwordSwingAudioClip); }

    public void PlayHeavySwordSwing() { _combatAudioSource.PlayOneShot(_heavySwordSwingAudioClip); }

    public void PlayHurtGrunt() { _combatAudioSource.PlayOneShot(_playerHurtAudioClip); }

    public void PlayAttackGrunt() { _combatAudioSource.PlayOneShot(_playerAttackGruntAudioClip); }

    public void PlayDeathScream() { _combatAudioSource.PlayOneShot(_playerDeathAudioClip); }

    public void PlayJump() { _jumpingAudioSource.Play(); }

}
