﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    private PlayerControler _player;
    private CapsuleCollider _collider;
    // Start is called before the first frame update
    void Start()
    {
        _player = GetComponentInParent<PlayerControler>();
        if(_player == null)
        {
            Debug.LogError("(Character)Start: Player is null");
        }

        _collider = GetComponent<CapsuleCollider>();
        if (_collider == null)
            Debug.LogError("(Character)Start: The Capsule Collider is NULL");
    }

    public void TakeDamage(int damage, Vector3 knockback)
    {
        GetComponent<CapsuleCollider>().enabled = false;
        _player.TakeDamage(damage, knockback);        
    }
    public void KnockbackDone()
    {
        GetComponent<CapsuleCollider>().enabled = true;
    }
}
