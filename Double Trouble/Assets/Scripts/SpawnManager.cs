﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    /// <summary>
    /// Array of all the tiles in the level.
    /// </summary>
    [SerializeField] private GameObject[] _tiles;
    [SerializeField] private GameObject _enemyPrefab;
    /// <summary>
    /// The position offset where the enemies will be spawned at.
    /// </summary>
    [SerializeField] private Vector3 _enemySpawnOffset = new Vector3(-1f, 1.2f, 0f);
    [SerializeField] private float _enemySpawnTimer = 1f;

    private void Start()
    {
        SpawnEnemies();
    }
    /// <summary>
    /// Spawn enemies ontop of all the tiles in the level
    /// </summary>
    private void SpawnEnemies()
    {
        int tileID = 0;
        foreach(GameObject tile in _tiles)
        {            
            SpawnEnemyAtTile(tileID);
            tileID++;
        }      
    }
    /// <summary>
    /// Spawn an enemy ontop of a tile.
    /// This actually just calls the Enemy Spawning Coroutine with the tileID as parameter.
    /// </summary>
    /// <param name="tileID">
    /// The ID of the tile that the enemy will spawn ontop of
    /// </param>
    private void SpawnEnemyAtTile(int tileID)
    {
        StartCoroutine(SpawnEnemyAtTileRoutine(tileID));
    }
    /// <summary>
    /// The Routine to spawn an enemy ontop of a Tile
    /// </summary>
    /// <param name="tileID">
    /// The ID of the Tile that the enemy will spawn ontop of
    /// </param>
    /// <returns></returns>
    IEnumerator SpawnEnemyAtTileRoutine(int tileID)
    {
        GameObject tile = _tiles[tileID];
        yield return new WaitForSeconds(_enemySpawnTimer);
        GameObject enemy = Instantiate(_enemyPrefab, tile.transform.position + _enemySpawnOffset, Quaternion.identity);
        enemy.name = "Enemy " + tileID;
        enemy.GetComponent<Enemy>().parentTileID = tileID;
        enemy.GetComponent<Enemy>().AddListener(SpawnEnemyAtTile);
    }
}
