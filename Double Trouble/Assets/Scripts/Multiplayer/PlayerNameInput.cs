﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class PlayerNameInput : MonoBehaviour
{
    //Script made following along the youtube tutorial of Dapper Dino https://www.youtube.com/playlist?list=PLS6sInD7ThM1aUDj8lZrF4b4lpvejB2uB

    //Two UI elements used to get and confirm player name.
    [Header("UI")]
    [SerializeField] private TMP_InputField nameInputField = null;
    [SerializeField] private Button continueButton = null;

    //Grab and save for display name.
    public static string DisplayName { get; private set; }

    private const string PlayerPrefsNameKey = "PlayerName";

    //Setts up input field with previously saved name (If there is one)
    private void Start() => SetUpInputField();

    private void SetUpInputField()
    { //If there is no previous name, return, if ther is a saved name, use that.
        if (!PlayerPrefs.HasKey(PlayerPrefsNameKey)) { return; }

        string defaultName = PlayerPrefs.GetString(PlayerPrefsNameKey);

        nameInputField.text = defaultName;

        SetPlayerName(defaultName);
    }
    //Makes button interactable only if the string isn't null.
    public void SetPlayerName(string name)
    {
        continueButton.interactable = !string.IsNullOrEmpty(name);
    }

    //Saves player name in PlayerPrefs.
    public void SavePlayerName()
    {
        DisplayName = nameInputField.text;

        PlayerPrefs.SetString(PlayerPrefsNameKey, DisplayName);
    }
}
