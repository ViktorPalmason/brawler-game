﻿using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManagerDoubleTrouble : NetworkManager
{
    //Script made following along the youtube tutorial of Dapper Dino https://www.youtube.com/playlist?list=PLS6sInD7ThM1aUDj8lZrF4b4lpvejB2uB

    //Number of players required to start the game.
    [SerializeField] private int minPlayers = 2;
    //Reference to the lobby scene.
    [Scene] [SerializeField] private string menuScene = string.Empty;

    //References to the Room and Game prefebs.
    [Header("Room")]
    [SerializeField] private NetworkRoomPlayerDoubleTrouble roomPlayerPrefab = null;
    
    [Header("Game")]
    [SerializeField] private NetworkGamePlayerDoubleTrouble gamePlayerPrefab = null;
    [SerializeField] private GameObject playerSpawnSystem = null;
    

    //Pubblic listeners for Client connected and disconnected.
    public static event Action OnClientConnected;
    public static event Action OnClientDisconnected;
    //Listeners for everyone connecting on scenechange.
    public static event Action<NetworkConnection> OnServerReadied;
    public static event Action OnServerStopped;

    //List of room players, so every client can loop over and fetch the data of other players in the lobby.
    public List<NetworkRoomPlayerDoubleTrouble> RoomPlayers { get; } = new List<NetworkRoomPlayerDoubleTrouble>();
    //Same as above, but for game players.
    public List<NetworkGamePlayerDoubleTrouble> GamePlayers { get; } = new List<NetworkGamePlayerDoubleTrouble>();

    //Pre-loads the assets for use in multiplayer. Loads them from Resources/SpawnablePrefabs
    public override void OnStartServer() => spawnPrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs").ToList();
    public override void OnStartClient()
    {
        var spawnablePrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs");

        foreach (var prefab in spawnablePrefabs)
        {
            ClientScene.RegisterPrefab(prefab);
        }
    }

    //Called on client when they connect to server. 
    public override void OnClientConnect(NetworkConnection conn)
    {
        //Do base logic.
        base.OnClientConnect(conn);
        //Raise event.
        OnClientConnected?.Invoke();
    }

    //The same as above, but for disconnecting.
    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);

        OnClientDisconnected?.Invoke();
    }

    //Called on server when client connects.
    public override void OnServerConnect(NetworkConnection conn)
    {
        //Checks if we have reached the max amount of players and disconnects client if thats the case.
        if (numPlayers >= maxConnections)
        {
            conn.Disconnect();
            return;
        }
        //If the server is not in the multiplayer menu scene, also disconnect client.
        if (SceneManager.GetActiveScene().path != menuScene)
        {
            conn.Disconnect();
            return;
        }
    }

    //Called on server when client tries to add new player.
    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        //If we're in the menu scene then spawn in connecting player.
        if (SceneManager.GetActiveScene().path == menuScene)
        {
            //If the player is the one that creates the server, we set them as leader.
            bool isLeader = RoomPlayers.Count == 0;

            NetworkRoomPlayerDoubleTrouble roomPlayerInstance = Instantiate(roomPlayerPrefab);

            roomPlayerInstance.IsLeader = isLeader;

            NetworkServer.AddPlayerForConnection(conn, roomPlayerInstance.gameObject);
        }
    }

    //Runs on server if a client disconnects.
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        //Fetches the disconnecting players room player script and removes it from the list of players in the lobby.
        if (conn.identity != null)
        {
            var player = conn.identity.GetComponent<NetworkRoomPlayerDoubleTrouble>();

            RoomPlayers.Remove(player);

            NotifyPlayersOfReadyState();
        }
        //Then destroys player on connection.
        base.OnServerDisconnect(conn);
    }

    //Called for everyone if the Server is stopped.
    public override void OnStopServer()
    {
        OnServerStopped?.Invoke();

        RoomPlayers.Clear();
        GamePlayers.Clear();
    }

    //Loops over all players and checks if they'r ready to start the game.
    public void NotifyPlayersOfReadyState()
    {
        foreach (var player in RoomPlayers)
        {
            player.HandleReadyToStart(IsReadyToStart());
        }
    }

    //If the number of players is under the minimum amount of players, or the number of players isn't the same as numbers of ready players, return false, else, return true.
    private bool IsReadyToStart()
    {
        if (numPlayers < minPlayers) { return false; }

        foreach (var player in RoomPlayers)
        {
            if (!player.IsReady) { return false; }
        }

        return true;
    }


    public void StartGame()
    {
        //If the scene we're in matches the scene we should be in, continue.
        if (SceneManager.GetActiveScene().path == menuScene)
        {
            //If we're ready to start, move to the game scene, if not, return.
            if (!IsReadyToStart()) { return; }

            //mapHandler = new MapHandler(mapSet, numberOfRounds);
            Debug.Log("Running Start function...");
            ServerChangeScene("MultiplayerScene_Map1");
        }
    }

    public override void ServerChangeScene(string newSceneName)
    {
        // From menu to game
        if (SceneManager.GetActiveScene().path == menuScene && newSceneName.Contains("MultiplayerScene"))
        {
            for (int i = RoomPlayers.Count - 1; i >= 0; i--)
            {
                //Get players connection.
                var conn = RoomPlayers[i].connectionToClient;
                //Spawn in the games prefabs
                var gameplayerInstance = Instantiate(gamePlayerPrefab);
                //Set display name from the room player to the game player.
                gameplayerInstance.SetDisplayName(RoomPlayers[i].DisplayName);

                //Destroys Room player and gives controll over a game player instead.
                NetworkServer.Destroy(conn.identity.gameObject);
                NetworkServer.ReplacePlayerForConnection(conn, gameplayerInstance.gameObject, true);

            }
        }
        //Then do baselogic for changing scene.
        base.ServerChangeScene(newSceneName);
    }

    //Called on server when a scene is completed.
    public override void OnServerSceneChanged(string sceneName)
    {
        //If it is a game map we spawn in the player spawn system.
        if (sceneName.Contains("MultiplayerScene"))
        {
            GameObject playerSpawnSystemInstance = Instantiate(playerSpawnSystem);
            NetworkServer.Spawn(playerSpawnSystemInstance);

        }
    }

    //Listens on when a client is ready (but called on server).
    public override void OnServerReady(NetworkConnection conn)
    {
        base.OnServerReady(conn);

        OnServerReadied?.Invoke(conn);
    }
}
