﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkGamePlayerDoubleTrouble : NetworkBehaviour
{
    //Script made following along the youtube tutorial of Dapper Dino https://www.youtube.com/playlist?list=PLS6sInD7ThM1aUDj8lZrF4b4lpvejB2uB

    [SyncVar]
    private string displayName = "Loading...";

    private NetworkManagerDoubleTrouble room;
    private NetworkManagerDoubleTrouble Room
    {
        get
        {
            if (room != null) { return room; }
            return room = NetworkManager.singleton as NetworkManagerDoubleTrouble;
        }
    }

    //Adds player to gameplayer list.
    public override void OnStartClient()
    {
        DontDestroyOnLoad(gameObject);
        Room.GamePlayers.Add(this);

    }

    //Having problem with this after Version change, will look into if time.
    /*public override void OnNetworkDestroy()
    {
        Room.GamePlayers.Remove(this);

    }*/



    [Server]
    public void SetDisplayName(string displayname)
    {
        this.displayName = displayname;
    }
}
