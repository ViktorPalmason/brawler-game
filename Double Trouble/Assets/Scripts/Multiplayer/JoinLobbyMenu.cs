﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class JoinLobbyMenu : MonoBehaviour
{
    //Script made following along the youtube tutorial of Dapper Dino https://www.youtube.com/playlist?list=PLS6sInD7ThM1aUDj8lZrF4b4lpvejB2uB

    //Reference to network manager.
    [SerializeField] private NetworkManagerDoubleTrouble networkManager = null;

    //Reference to UI elements.
    [Header("UI")]
    [SerializeField] private GameObject landingPagePanel = null;
    [SerializeField] private TMP_InputField ipAddressInputField = null;
    [SerializeField] private Button joinButton = null;

    //Calls on Connection to a server.
    private void OnEnable()
    {
        NetworkManagerDoubleTrouble.OnClientConnected += HandleClientConnected;
        NetworkManagerDoubleTrouble.OnClientDisconnected += HandleClientDisconnected;
    }
    //Calls on disconnect from a server.
    private void OnDisable()
    {
        NetworkManagerDoubleTrouble.OnClientConnected -= HandleClientConnected;
        NetworkManagerDoubleTrouble.OnClientDisconnected -= HandleClientDisconnected;
    }

    //Function for join lobby button.
    public void JoinLobby()
    {
        //Tries to join the server with the provided IP adress (Joins as a client).
        string ipAddress = ipAddressInputField.text;

        networkManager.networkAddress = ipAddress;
        networkManager.StartClient();
        //Disables join button to avoid server crash by join-request spamming.
        joinButton.interactable = false;
    }

    //Function runs if we join a server.
    private void HandleClientConnected()
    {
        //We enable join button for the next time player tries to join a server, and we deselct the Join Server UI.
        joinButton.interactable = true;

        gameObject.SetActive(false);
        landingPagePanel.SetActive(false);
    }

    //Function runs if we can't find server, it activates join button so player can attempt to join another server.
    private void HandleClientDisconnected()
    {
        joinButton.interactable = true;
    }
}
