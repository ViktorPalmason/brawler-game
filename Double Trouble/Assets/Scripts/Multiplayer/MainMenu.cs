﻿using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private NetworkManagerDoubleTrouble networkManager = null;
    //UI element we controll in script.
    [Header("UI")]
    [SerializeField] private GameObject landingPagePanel = null;

    //When we want to host a lobby, we start host.
    public void HostLobby()
    {
        networkManager.StartHost();
        //Disables landing page after hosting.
        landingPagePanel.SetActive(false);
    }
}
