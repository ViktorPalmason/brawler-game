﻿using Mirror;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class PlayerSpawnSystem : NetworkBehaviour
{
    //Reference to player prefab.
    [SerializeField] private GameObject playerPrefab = null;

    //List transforms in the scene.
    private static List<Transform> spawnPoints = new List<Transform>();

    //Increment of players joined.
    private int nextIndex = 0;

    //Adds spawnpoint in scene.
    public static void AddSpawnPoint(Transform transform)
    {
        spawnPoints.Add(transform);

        spawnPoints = spawnPoints.OrderBy(x => x.GetSiblingIndex()).ToList();
    }
    
    //Removes spawnpoint.
    public static void RemoveSpawnPoint(Transform transform) => spawnPoints.Remove(transform);


    public override void OnStartServer() => NetworkManagerDoubleTrouble.OnServerReadied += SpawnPlayer;


    [ServerCallback]
    private void OnDestroy() => NetworkManagerDoubleTrouble.OnServerReadied -= SpawnPlayer;

    [Server]
    public void SpawnPlayer(NetworkConnection conn)
    { //Gets spawnpoint for each player based on their index.
        Transform spawnPoint = spawnPoints.ElementAtOrDefault(nextIndex);

        //Error if we have too few or are missing a spawnpoint.
        if (spawnPoint == null)
        {
            Debug.LogError($"Missing spawn point for player {nextIndex}");
            return;
        }
        //Spawns player at correct place facing the correct way.
        GameObject playerInstance = Instantiate(playerPrefab, spawnPoints[nextIndex].position, spawnPoints[nextIndex].rotation);
        //Spawns on all clients.
        NetworkServer.AddPlayerForConnection(conn, playerInstance);

        nextIndex++;
    }
}
