﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : MonoBehaviour
{
    [SerializeField] private Text _damageValue;
    [SerializeField] private float _textSpeed = 1f;
    [SerializeField] private float _lifeTime = 0.75f;

    private void Start()
    {
        StartCoroutine(RemoveTextRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * _textSpeed * Time.deltaTime);  
    }
    public void SetDamageText(string damageValue)
    {
        _damageValue.text = damageValue;
    }

    IEnumerator RemoveTextRoutine()
    {
        yield return new WaitForSeconds(_lifeTime);
        Destroy(gameObject);
    }
}
