﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private GameObject _damageTextContainer;
    [SerializeField] private GameObject _damageTextPrefab;
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _gameOverText;
    [SerializeField] private Text _deathConditionText;
    [SerializeField] private Text _winConditionText;
    private GameManager _gameManager;

    private void Start()
    {
        _gameOverText.gameObject.SetActive(false);
        _deathConditionText.gameObject.SetActive(false);
        _winConditionText.gameObject.SetActive(false);

        _gameManager = GameObject.Find("Game_Manager").GetComponent<GameManager>();
        if (_gameManager == null)
            Debug.LogError("(PlayerUI)Start: The Game Manager is NULL");
    }
    public void AddChildDamageText(string damage)
    {
        GameObject text = Instantiate(_damageTextPrefab, transform.position, Quaternion.identity, _damageTextContainer.transform);
        text.GetComponent<DamageText>().SetDamageText(damage);
    }

    public void UpdateScoreText(string score)
    {
        _scoreText.text = "Score: " + score;
    }

    public void EndGameSequence(string condition)
    {
        _gameOverText.gameObject.SetActive(true);
        Debug.Log(condition);
        switch (condition)
        {
            case "Death":
                _deathConditionText.gameObject.SetActive(true);
                break;
            case "Win":
                _winConditionText.gameObject.SetActive(true);
                break;
            default:
                break;
        }

        _gameManager.GameOver();
    }
}
