﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{

     public GameObject singleplayerPanel, multiplayerPanel;

    public void goToSingleplayer()
    {
        SceneManager.LoadScene("SinglePlayerScene");
    }

    public void goToMultiplayer()
    {
        SceneManager.LoadScene("MultiplayerLobbyScene");
    }

    public void singleplayerTextPaneloff()
    {
        singleplayerPanel.SetActive(false);
    }
    public void singleplayerTextPanelon()
    {
        singleplayerPanel.SetActive(true);
    }

    public void multiplayerTextPaneloff()
    {
        multiplayerPanel.SetActive(false);
    }
    public void multiplayerTextPanelon()
    {
        multiplayerPanel.SetActive(true);
    }

    public void exit()
    {
        Debug.Log("Closing application (Doesn't work in editor.)");
        Application.Quit();
    }
}
