﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private Transform _camera;

    private void Start()
    {
        _camera = GameObject.Find("Main Camera").GetComponent<Transform>();
        if(_camera == null)
        {
            Debug.LogError("(Billboard)Start: The Main Camera is NULL");
        }
    }

    void LateUpdate()
    {
        transform.LookAt(transform.position + _camera.forward);
    }
}
