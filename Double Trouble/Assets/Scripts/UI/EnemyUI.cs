﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyUI : MonoBehaviour
{
    [SerializeField] private GameObject _damageTextContainer;
    [SerializeField] private GameObject _damageTextPrefab;
    [SerializeField] public Healthbar healthbar;
    public void AddChildDamageText(string damage)
    {
        GameObject text = Instantiate(_damageTextPrefab, transform.position, Quaternion.identity, _damageTextContainer.transform);
        text.GetComponent<DamageText>().SetDamageText(damage);
    }

}
