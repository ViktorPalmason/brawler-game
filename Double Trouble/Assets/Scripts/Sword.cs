﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Represents the swords in the game
/// </summary>
public class Sword : MonoBehaviour
{
    [SerializeField] private int _lightAttackMinDamage = 2;
    [SerializeField] private int _lightAttackMaxDamage = 4;
    [SerializeField] private int _heavyAttackMinDamage = 5;
    [SerializeField] private int _heavyAttackMaxDamage = 7;
    [SerializeField] private float _knockbackForce = 4;

    private MeshCollider _meshCollider;
    /// <summary>
    /// A bool that tells if the player is performing the light attack.
    /// </summary>
    private bool _lightAttack = false;
    /// <summary>
    /// A bool that tells if the player is performing the heavy attack.
    /// </summary>
    private bool _heavyAttack = false;
    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _meshCollider = GetComponent<MeshCollider>();
        if(_meshCollider == null)
        {
            Debug.LogError("(Sword)Start: Mesh collider is NULL");
        }
        _audioSource = GetComponent<AudioSource>();
        if(_audioSource == null)
        {
            Debug.LogError("(Sword)Start: The Audio Source is NULL");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            if (_lightAttack)
            {
                /// <remarks>
                /// _lightAttackMaxDamage is given a +1 since Range() with ints is exclusive with the max parameter
                /// so to have the Range method include the _lightAttackMaxDamage value we give it a +1;
                /// </remarks>
                int damage = Random.Range(_lightAttackMinDamage, _lightAttackMaxDamage+1);
                Vector3 knockback = new Vector3(other.transform.position.x - transform.position.x,
                                                other.transform.position.y - transform.position.y,
                                                0).normalized;
                //Debug.Log("(Light Attack)knockback: " + knockback);
                other.gameObject.GetComponent<Enemy>().TakeDamage(damage, knockback, _knockbackForce);
                DisableMeshCollider();
                _audioSource.Play();
            }
            else if (_heavyAttack)
            {
                /// <remarks>
                /// _heavyAttackMaxDamage is given a +1 since Range() with ints is exclusive with the max parameter
                /// so to have the Range method include the _heavyAttackMaxDamage value we give it a +1;
                /// </remarks>
                int damage = Random.Range(_heavyAttackMinDamage, _heavyAttackMaxDamage+1);
                Vector3 knockback = new Vector3(other.transform.position.x - transform.position.x,
                                                other.transform.position.y - transform.position.y,
                                                0).normalized;
                //Debug.Log("(Heavy Attack)knockback: " + knockback);
                other.gameObject.GetComponent<Enemy>().TakeDamage(damage, knockback, _knockbackForce);
                DisableMeshCollider();
                _audioSource.Play();
            }
        }            
    }


    public void EnableMeshCollider()
    {
        if(_meshCollider != null)
        {
            _meshCollider.enabled = true;
        }
        else
        {
            Debug.LogError("(Sword)EnableMeshCollider: The sword Mesh collider is NULL");
        }
    }
    public void DisableMeshCollider()
    {
        if(_meshCollider != null)
        {
            _meshCollider.enabled = false;
        }
        else
        {
            Debug.LogError("(Sword)DisableMeshCollider: The sword Mesh collider is NULL");
        }
    }
    public void AttackActivate(string attackType)
    {
        switch (attackType)
        {
            case "LightAttack":
                _lightAttack = true;
                //EnableMeshCollider();
                break;
            case "HeavyAttack":
                _heavyAttack = true;
                //EnableMeshCollider();
                break;
            default:
                break;
        }
        EnableMeshCollider();
    }
    public void AttackDeactivate(string attackType)
    {
        switch (attackType)
        {
            case "LightAttack":
                _lightAttack = false;
                //DisableMeshCollider();
                break;
            case "HeavyAttack":
                _heavyAttack = false;
                //DisableMeshCollider();
                break;
            default:
                break;
        }
        DisableMeshCollider();
    }
}
