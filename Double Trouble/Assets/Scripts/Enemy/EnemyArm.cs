﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Represents the arms of an enemy which act as the enemy weapons.
/// </summary>
public class EnemyArm : MonoBehaviour
{
    [SerializeField] private int _minDamage = 1;
    [SerializeField] private int _maxDamage = 3;
    /// <summary>
    /// The number of hits the enemy arm has done
    /// </summary>
    private int _hits;
    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        if(_audioSource == null)
        {
            Debug.LogError("(EnemyArm)Start: The Audio Source is NULL");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        /// <remarks>
        /// This tag is on the 'Character' object which is child to the 'Player' object. 
        /// The reason collision detection is done on the 'Character' is because it has the correct
        /// position data when calculating the knockback.
        /// </remarks>
        if(other.tag == "Player_body")
        {
            _audioSource.Play();
            /// <remarks>
            /// _maxDamage is given a +1 since Range() with ints is exclusive with the max parameter
            /// so to have the Range method include the _maxDamage value we give it a +1;
            /// </remarks>
            int damage = Random.Range(_minDamage, _maxDamage+1);
            _hits++;
            Vector3 knockback = new Vector3(other.transform.position.x - transform.position.x,
                                            other.transform.position.y - transform.position.y,
                                            0).normalized;
            //Debug.Log("(EnemyArm)knockback: " + knockback);
            other.gameObject.GetComponent<Character>().TakeDamage(damage, knockback);
            
        }
    }
}
