﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _movementSpeed = 1f;
    [SerializeField] private float _rotationSpeed = 100f;
    [SerializeField] private int _maxHealth = 10;
    [SerializeField] private int _currentHealth;
    [SerializeField] private int _killValue = 10;
    /// <summary>
    /// The offset from the starting position that sets end position of the enemy position interpolation.
    /// </summary>
    /// <remarks>
    /// Is used when calculating the ending position for the enemy movement interpolation 
    /// by adding this with the starting position.
    /// </remarks>
    [SerializeField] private Vector3 _interpolationOffset = new Vector3(4f, 0f, 0f);
    [SerializeField] private EnemyUI _UI;
    [SerializeField] private AudioClip _deathAudioClip;
    [SerializeField] private AudioClip _hurtAudioClio;

    /// <summary>
    /// The start position in the enemy position interpolation
    /// </summary>
    /// <remarks>
    /// This is the enemies spawning position
    /// </remarks>
    private Vector3 _startPos;
    /// <summary>
    /// The end position in the enemy position interpolation.
    /// </summary>
    private Vector3 _endPos;
    /// <summary>
    /// The fraction of the distance between the starting and end position that the enemy is at currently 
    /// in the interpolation.
    /// </summary>
    private float _fraction = 0f;
    private PlayerControler _player;
    private Rigidbody _rb;
    private AudioSource _audioSource;
    private Animator _anim;
    private bool _isAlive;

    public delegate void OnDeathEventHandler(int number);
    public event OnDeathEventHandler OnDeath;
    /// <summary>
    /// This is to store the Enemy spawning method from the Spawn Manager so its possible
    /// to subscribe and unsubsribe it from the OnDeath event.
    /// </summary>
    private OnDeathEventHandler _spawningMethod;
    /// <summary>
    /// This is the ID of the Tile that the enemy spawn ontop of
    /// </summary>
    public int parentTileID { private get; set; }

    void Start()
    {
        _currentHealth = _maxHealth;
        _isAlive = true;
        
        _player = GameObject.Find("Player").GetComponent<PlayerControler>();
        if(_player == null)
            Debug.LogError("(" + name + ")Start: Player is NULL");

        _rb = GetComponent<Rigidbody>();
        if(_rb == null)
            Debug.LogError("(" + name + ")Start: Rigidbody is NULL");

        if(_UI == null)
        {
            Debug.LogError("(" + name + ")Start: UI is Null");
        }
        else
        {
            _UI.healthbar.SetMaxHealth(_maxHealth);
        }

        _audioSource = GetComponent<AudioSource>();
        if(_audioSource == null)
            Debug.LogError("(Enemy)Start: The Audio Source is NULL");

        _startPos = transform.position;
        _endPos = transform.position + _interpolationOffset;
    }
    // Update is called once per frame
    void Update()
    {
        //InterpolateEnemy();

        if (_isAlive)
        {
            _rb.transform.Rotate(Vector3.up * _rotationSpeed * Time.deltaTime);
        }
    }
    /// <summary>
    /// Interpolating the enemy position between its starting position, i.e. spawning position,
    /// and an end position that is set by _interpolationOffset <see cref="Enemy._interpolationOffset"/>.
    /// </summary>
    private void InterpolateEnemy()
    {
        // swap the value sign of the movement speed so it goes right and left after it has
        // reached either the begin position or end position.
        if (_fraction >= 1f)
        {
            _fraction = 1f;
            _movementSpeed *= -1f;
        }
        else if(_fraction <= 0)
        {
            _fraction = 0f;
            _movementSpeed *= -1f;
        }

        _fraction += Time.deltaTime * _movementSpeed;
        _rb.transform.position = Vector3.Lerp(_startPos, _endPos, _fraction);
    }
    public void TakeDamage(int damage, Vector3 knockback, float force)
    {
        if (_isAlive)
        {
            _currentHealth -= damage;
            _UI.healthbar.SetHealth(_currentHealth);
            _UI.AddChildDamageText(damage.ToString());
            if (_currentHealth <= 0)
            {
                //Sending the x value to dermine the direction of the attack so the enemy falls correctly.
                EnemyIsKilled(knockback);
                _rb.AddForce(knockback * _rb.mass * force, ForceMode.Impulse);
            }
            else
            {
                _audioSource.Play();
                _rb.AddForce(knockback * _rb.mass * force, ForceMode.Impulse);
            }
        }
    }
    private void EnemyIsKilled(Vector3 knock)
    {
        _isAlive = false;
        _player.GivePlayerPoints(_killValue);

        if (OnDeath != null)
            OnDeath.Invoke(parentTileID);
        else
            Debug.LogError("(" + name + ")EnemyIsKilled: OnDeath is NULL -> OnDeath.Invoke(_tileID)");

        if(_spawningMethod != null)
            OnDeath -= _spawningMethod;
        else
            Debug.LogError("(" + name + ")EnemyIsKilled: _spawningMethod is NULL -> When trying to Unsubscribe from the OnDeath event");
        /// To make sure there are no methods subscribed to the event to prevent memory leaks.
        if (OnDeath == null)
        {
            _audioSource.PlayOneShot(_deathAudioClip, 1f);
            StartCoroutine(EnemyDeathRoutine(knock));
        }
        else
        {
            Debug.LogError("(" + name + ")EnemyIsKilled: OnEnemyDeath is NOT NULL -> In the Null check before destroying the enemy object:");
        }
    }

    IEnumerator EnemyDeathRoutine(Vector3 knock)
    {
        int count = 0;

        while(count < 80)
        {
            _rb.transform.Rotate(knock * _rotationSpeed * Time.deltaTime);
            count++;
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(this.gameObject);
    }
    public void AddListener(OnDeathEventHandler method)
    {
        _spawningMethod = method;
        OnDeath += _spawningMethod;
    }
}
